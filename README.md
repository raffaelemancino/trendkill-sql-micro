<p align="center">
    <a href="https://bitbucket.org/raffaelemancino/trendkill-sql-micro" target="blank">
        <img src="https://bitbucket.org/raffaelemancino/trendkill-sql-micro/raw/9866295644021ee62f4439a80faf621d08248394/snake.png" width="150" alt="Trendkill Logo" />
    </a>
</p>

Trendkill
=========

### Description

Trendkill is an agnostic RESTful Microservice, for SQL DB.
It supports MySQL, MariaDB and PostgreSQL.
Use **mysql** for MySQL and MariaDB and **pg** for PostgreSQL

### Installation

```bash
$ npm install
$ npm build
$ docker build . -t trendkill
$ docker run 
  -e DB_TYPE=mysql 
  -e DB_HOST=127.0.0.1 
  -e DB_PORT=3306
  -e DB_USERNAME=root
  -e DB_PASSWORD=root
  -e DB_DATABASE=world
  -d trendkill
  
```

### Running the app

```bash
# development
$ npm run start

# watch mode
$ npm run start:dev

# production mode
$ npm run start:prod
```

[comment]: <> (### Test)

[comment]: <> (```bash)

[comment]: <> (# unit tests)

[comment]: <> ($ npm run test)

[comment]: <> (# e2e tests)

[comment]: <> ($ npm run test:e2e)

[comment]: <> (# test coverage)

[comment]: <> ($ npm run test:cov)

[comment]: <> (```)

### Support

Trendkill is an GPLv2-licensed open source project. It can grow thanks to the sponsors and support by the amazing backers. If you'd like to join them.

### Stay in touch

Author - [Raffaele F. Mancino](https://bitbucket.org/raffaelemancino/)

### License

Nest is [GNU GPLv2 licensed](LICENSE).
